from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login as auth_login,logout
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_list_or_404, get_object_or_404
from .forms import *
from django.contrib import messages
from .models import *
from .decorators import unauthenticated_user
from django.core.files.storage import FileSystemStorage


# Create your views here.

@unauthenticated_user
def loginpage(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(request,email=email,password=password)
        if user.is_active:
            auth_login(request, user)
            return redirect('index')
        else:
            messages.success(request,'Email Or Password is incorrect')
    context = {}
    return render(request,'resume/login.html',context)

@unauthenticated_user
def register(request):
    form = CreateUserForm()
    #profile = ProfileForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        #profile = ProfileForm(request.POST)
        #if profile.is_valid():
        #    profile.save()
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            messages.success(request,'Account was Created Succesfully for '+  email)
            return redirect('login')
    context = {'form':form}
    return render(request,'resume/register.html',context)

def logoutuser(request):
    logout(request)
    return redirect('all')

#--------------------------------------------------------------#

@login_required(login_url='/login/')
def index(request):
    if request.user.is_authenticated:
        user = request.user.id
        profile = Profile.objects.get(id=user)
        socialmedias = profile.socialmedia_set.all()
        perso = profile.personal_set.all()
    context = {'profile':profile,'socialmedias':socialmedias,'perso':perso}
    return render(request,'resume/index.html',context)

@login_required(login_url='/login/')
def about(request):
    if request.user.is_authenticated:
        user = request.user.id
        profile = Profile.objects.get(id=user)
        quali = profile.qualification_set.all()
        skills = profile.skill_set.all()
        hobi = profile.hobbie_set.all()
        perso = profile.personal_set.all()
        socialmedias = profile.socialmedia_set.all()
        background_home = profile.backgroundimage_set.all()
        other_pic = profile.otherimage_set.all()
        context = {
            'profile':profile,
            'quali':quali,
            'skills':skills,
            'hobi':hobi,
            'perso':perso,
            'socialmedias':socialmedias,
            'background_home':background_home,
            'other_pic':other_pic
            }
    return render(request,'resume/about.html',context)

@login_required(login_url='/login/')
def profileupdate(request): 
    if request.user.is_authenticated:
        id = request.user.profile.id 
        profile = Profile.objects.get(id=id)
        form = ProfileForm(instance=profile)
        #user1 = User.objects.get(id=profile.id)
        #form1= UserCreationForm(instance=user1)
        if request.method == "POST":
            form = ProfileForm(request.POST or None,request.FILES ,instance=profile)
            #form1= UserCreationForm(request.POST or None, instance=user1)
        if form.is_valid():
            form.save()
            messages.success(request,'Update Successful')
            return redirect('./')
    context = {'form':form,'profile':profile}
    return render(request,'resume/settings/profileupdate.html',context)
    
@login_required(login_url='/login/')
def aboutupdate(request):
    if request.user.is_authenticated:
        id = request.user.profile.id
        profile = Profile.objects.get(id=id)

        quali = Qualification.objects.get(id=profile.id)
        skill = Skill.objects.get(id=profile.id)
        perso = Personal.objects.get(id=profile.id)
        hobi = Hobbie.objects.get(id=profile.id)
        socialmedia = Socialmedia.objects.get(id=profile.id)


        form = QualiForm(instance=quali)
        form2 = PersoForm(instance=perso)
        form3 = SkillForm(instance=skill)
        form4 = HobiForm(instance=hobi)
        form5 = SocialForm(instance=socialmedia)

        if request.method == "POST":
            form = QualiForm(request.POST or None,instance=quali)
            form2 = PersoForm(request.POST or None,instance=perso)
            form3 = SkillForm(request.POST or None,instance=skill)
            form4 = HobiForm(request.POST or None,instance=hobi)
            form5 = SocialForm(request.POST or None,instance=socialmedia)
        if form.is_valid():
            form.save()
            messages.success(request,'Update Successful')
            return redirect('./')
        if form2.is_valid():
            form2.save()
            messages.success(request,'Update Successful')
            return redirect('./')
        if form3.is_valid():
            form3.save()
            messages.success(request,'Update Successful')
            return redirect('./')
        if form4.is_valid():
            form4.save()
            messages.success(request,' Update Successful')
            return redirect('./')
        if form5.is_valid():
            form5.save()
            messages.success(request,'Update Successful')
            return redirect('./')
    context = {
        'form':form,
        'form2':form2,
        'form3':form3,
        'form4':form4,
        'form5':form5,
        }
    return render(request,'resume/settings/aboutupdate.html',context)

@login_required(login_url='/login/')
def project(request):
    if request.user.is_authenticated:
        user = request.user.id
        profile = Profile.objects.get(id=user)
    context = {'profile':profile}
    return render(request,'resume/settings/projectupdate.html',context)

@login_required(login_url='/login/')
def bgpic(request):
    if request.user.is_authenticated:
        user = request.user.id
        profile = Profile.objects.get(id=user)
    context = {'profile':profile}
    return render(request,'resume/settings/bgpic.html',context)

#---------------------------------------------------------------#
@unauthenticated_user
def all(request):
    profile = Profile.objects.all()
    context = {'profile':profile}
    return render(request,'resume/all.html',context)

@unauthenticated_user
def indexs(request,pk):
    profile = Profile.objects.get(id=pk)
    aa = pk
    hobi = profile.hobbie_set.all()
    socialmedias = profile.socialmedia_set.all()
    perso = profile.personal_set.all()
    context = {'profile':profile,'hobi':hobi,'socialmedias':socialmedias,'perso':perso,'aa':aa}
    return render(request,'resume/index.html',context)

@unauthenticated_user
def abouts(request,pk):
    profile = Profile.objects.get(id=pk)
    quali = profile.qualification_set.all()
    skills = profile.skill_set.all()
    hobi = profile.hobbie_set.all()
    perso = profile.personal_set.all()
    socialmedias = profile.socialmedia_set.all()
    background_home = profile.backgroundimage_set.all()
    other_pic = profile.otherimage_set.all()
    context = {
            'profile':profile,
            'quali':quali,
            'skills':skills,
            'hobi':hobi,
            'perso':perso,
            'socialmedias':socialmedias,
            'background_home':background_home,
            'other_pic':other_pic
            }
    return render(request,'resume/about.html',context)

@unauthenticated_user
def contacts(request,pk):
    profile = Profile.objects.get(id=pk)
    hobi = profile.hobbie_set.all()
    context = {'user':profile,'hobi':hobi}
    return render(request,'resume/contact.html',context)