from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import *


class ProfileForm(ModelForm):
  class Meta:
    model = Profile
    fields = '__all__'
    exclude = ['user']

class QualiForm(ModelForm):
  class Meta:
    model = Qualification
    fields = '__all__'
    exclude = ['profile']

class SkillForm(ModelForm):
  class Meta:
    model = Skill
    fields = '__all__'
    exclude = ['profile']

class HobiForm(ModelForm):
  class Meta:
    model = Hobbie
    fields = '__all__'
    exclude = ['profile']

class PersoForm(ModelForm):
  class Meta:
    model = Personal
    fields = '__all__'
    exclude = ['profile' ,'created_date']
  
class SocialForm(ModelForm):
  class Meta:
    model =Socialmedia
    fields = '__all__'
    exclude = ['profile']

class CreateUserForm(UserCreationForm):
  class Meta:
    model = User
    fields = ['full_name','email','password1','password2']

  