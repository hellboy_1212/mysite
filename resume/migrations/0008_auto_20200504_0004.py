# Generated by Django 3.0.5 on 2020-05-03 18:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0007_auto_20200504_0002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personal',
            name='image',
            field=models.ImageField(blank=True, default="mysite/resume/static/images/default_user.png' %}", null=True, upload_to=None),
        ),
    ]
