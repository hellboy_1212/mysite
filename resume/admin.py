from django.contrib import admin
from resume.models import *

# Register your models here.
admin.site.register(Otherimage)
admin.site.register(Socialmedia)
admin.site.register(Backgroundimage)
admin.site.register(Personal)
admin.site.register(Hobbie)
admin.site.register(Skill)
admin.site.register(Qualification)
admin.site.register(Profile)
admin.site.register(User)