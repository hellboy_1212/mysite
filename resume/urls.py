from django.contrib import admin
from django.urls import path,include
from resume import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/',views.loginpage,name='login'),
    path('logout/',views.logoutuser,name='logout'),
    path('register/',views.register,name='register'),
    path('change-password',auth_views.PasswordChangeView,name='change-password'),
    #---------------------------------------------------#
    path('index/',views.index,name='index'),
    path('about/',views.about,name='about'),
    path('profile-update/',views.profileupdate,name='profileupdate'),
    path('about-update/',views.aboutupdate,name='aboutupdate'),
    path('projets/',views.project,name='project'),
    path('Background_pic_update/',views.bgpic,name='bgpic'),
#-------------------------------------------------------#
    
    path('',views.all,name='all'),
    path('home/<str:pk>',views.indexs,name='indexs'),
    path('about/<str:pk>',views.abouts,name='abouts'),
    path('contact/<str:pk>',views.contacts,name='contacts'),
]

