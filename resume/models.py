from django.db import models
from django.contrib.auth.models import(
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)
from django.utils import timezone
from django.conf import settings



class UserManager(BaseUserManager):
    def create_user(self,email,full_name,password = None,is_active=True,is_staff=False,is_admin=False):
        if not email:
            raise ValueError("User must have an email address")
        if not full_name:
            raise ValueError("User must have an full_name address")
        if not password:
            raise ValueError("User must have a password ")
        user_obj = self.model(
            email = self.normalize_email(email.lower()),
            full_name=full_name,
        )
        user_obj.set_password(password)
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.active = is_active
        user_obj.save(using=self._db)
        return user_obj
        
    def create_staffuser(self,full_name,email,password=None):
        user = self.create_user(
            email,
            full_name,
            password=password,
            is_staff=True
        )
        return user
    
    def create_superuser(self,full_name,email,password=None):
        user = self.create_user(
            email,
            full_name,
            password=password,
            is_admin=True,
            is_staff=True
        )
        return user

class User(AbstractBaseUser,PermissionsMixin):
    email = models.EmailField(max_length=100,unique=True)
    full_name = models.CharField(max_length =100,blank=False,null=False)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin
    
    def has_module_perms(self, app_label):
        return self.is_admin

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active

empty = 'Empty Form'
class Profile(models.Model):
    profile_default = 'default_user.png'
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    firstname = models.CharField(max_length = 50,default=empty,blank=True,null=True)
    lastname = models.CharField(max_length = 50,default=empty,blank=True,null=True)
    image = models.ImageField(default=profile_default,null=True,blank=True)
    occuption = models.CharField(max_length = 50,default=empty,blank=True,null=True)

    def __str__(self):
        return self.firstname

class Qualification(models.Model):
    STUDY_ON = (
        ('SELECT','SELECT'),
        ('PHARMACY','PHARMACY'),
        ('BE','BE'),
        ('BCA','BCA'),
        ('12','12'),
        ('10','10')
    )
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE,null=True)
    course = models.CharField(max_length = 150,null=True,choices=STUDY_ON,default='SELECT')
    year = models.DateField(auto_now=False, auto_now_add=False,null=True,default=empty)
    institute = models.CharField(max_length = 150,null=True,default=empty)
    board = models.CharField(max_length = 150,null=True,default=empty)
    marks = models.FloatField(null=True,default=empty)

    def __str__(self):
       return self.course

class Skill(models.Model):
    NEW = (
        ("Skills","Skill"),
        ("Pecertange","Pecertange"),
    )

    COLOR = (
        ('Select','Select'),
        ('warning','yellow'),
        ('success','green'),
        ('striped','light-blue'),
        ('primary','purple'),
        ('info','light-blur-green'),
        ('danger','danger'),
    )
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE,null=True)
    skill = models.CharField(max_length = 12,blank=True,null=True,default=empty)
    percentage = models.FloatField(default=0.0)
    select_colors = models.CharField(max_length=120,default='Select',choices=COLOR,null=True)
    
    #s1 = models.CharField(max_length = 12,blank=True,null=True)
    #p1 = models.FloatField(default=0.0)
    #s2 = models.CharField(max_length = 12,blank=True,null=True)
    #p2 = models.FloatField(default=0.0)
    #s3 = models.CharField(max_length = 12,blank=True,null=True)
    #p3 = models.FloatField(default=0.0)
    #s4 = models.CharField(max_length = 12,blank=True,null=True)
    #p4 = models.FloatField(default=0.0)
    #s5 = models.CharField(max_length = 12,blank=True,null=True)
    #p5 = models.FloatField(default=0.0)
    #s6 = models.CharField(max_length = 12,blank=True,null=True)
    #p6 = models.FloatField(default=0.0)
    #s7 = models.CharField(max_length = 12,blank=True,null=True)
    #p7 = models.FloatField(default=0.0)
    #s8 = models.CharField(max_length = 12,blank=True,null=True)
    #p8 = models.FloatField(default=0.0)

    def __str__(self):
        return self.skill
    
class Hobbie(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE,null=True)
    hobi1 = models.CharField(max_length = 150,blank=True,null=True,default=empty)
    hobi2 = models.CharField(max_length = 150,blank=True,null=True,default=empty)
    hobi3 = models.CharField(max_length = 150,blank=True,null=True,default=empty)
    hobi4 = models.CharField(max_length = 150,blank=True,null=True,default=empty)
    hobi5 = models.CharField(max_length = 150,blank=True,null=True,default=empty)
    hobi6 = models.CharField(max_length = 150,blank=True,null=True,default=empty)

    def __str__(self):
       return self.hobi1

class Personal(models.Model):
    NATIONALITY = (
        ('Select','Select'),
        ('Indian','Indian'),
        ('Nepali','Nepali'),
        ('Bhutani','Bhutani'),
        ('Pakistani','Pakistani'),
        ('Srilankani','Srilankani'),
    )

    STATU = (
        ('Select','Select'),
        ('Married','Married'),
        ('Single','Single'),
    )

    GENDERS = (
        ('Select','Select'),
        ('Male','Male'),
        ('Female','Female'),
    )
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE,null=True)
    nationality = models.CharField(max_length = 150,choices=NATIONALITY,default='Select')
    status = models.CharField(max_length = 150,choices=STATU,default='Select')
    gender = models.CharField(max_length = 150,choices=GENDERS,default='Select')
    about = models.TextField(max_length=300,default=empty)
    phone_number = models.BigIntegerField()
    address1 = models.TextField(max_length=150,default=empty)
    address2 = models.TextField(max_length=150,default=empty)
    created_date = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
       return self.gender
    
class Backgroundimage(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE,null=True)
    homebg = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=100,default='',null=True,blank=True)
    contentbg = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=100,default='',null=True,blank=True)
    aboutbg = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=100,default='',null=True,blank=True)

class Socialmedia(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE,null=True)
    facebook_link = models.URLField(blank=True,null=True,default=empty)
    instagram_link = models.URLField(blank=True,null=True,default=empty)
    twitter_link = models.URLField(blank=True,null=True,default=empty)
    linkedin_link = models.URLField(blank=True,null=True,default=empty)

class Otherimage(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE,null=True)
    photostore = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=100,default='',null=True,blank=True)


    
    
    
    